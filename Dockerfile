FROM docker.io/rockylinux/rockylinux:8 as buildenv

RUN dnf install -y \
    yum-utils \
    && dnf clean all

RUN yum-config-manager \
    --setopt=baseos.baseurl=http://nexus.home.nodeto.com:8081/repository/rocky/8.6/BaseOS/x86_64/os \
    --setopt=appstream.baseurl=http://nexus.home.nodeto.com:8081/repository/rocky/8.6/AppStream/x86_64/os \
    --setopt=extras.baseurl=http://nexus.home.nodeto.com:8081/repository/rocky/8.6/extras/x86_64/os \
    --setopt=powertools.baseurl=http://nexus.home.nodeto.com:8081/repository/rocky/8.6/PowerTools/x86_64/os \
    --setopt=baseos.mirrorlist= \
    --setopt=appstream.mirrorlist= \
    --setopt=extras.mirrorlist= \
    --setopt=powertools.mirrorlist= \
    --save

RUN yum-config-manager --enable powertools

RUN dnf install -y \
    rpm-build \
    rpmdevtools \
    dnf-utils \
    yum-utils \
    && dnf clean all

RUN rpmdev-setuptree

RUN curl http://nexus.home.nodeto.com:8081/repository/generic/python/python-dotenv-0.20.0.tar.gz \
    -o /root/rpmbuild/SOURCES/python-dotenv-0.20.0.tar.gz

COPY rpm.spec /root/rpmbuild/SPECS/
RUN yum-builddep -y /root/rpmbuild/SPECS/rpm.spec
RUN rpmbuild -ba --noclean /root/rpmbuild/SPECS/rpm.spec

ARG YUM_USER
ARG YUM_PASSWORD
RUN curl -f -u ${YUM_USER}:${YUM_PASSWORD} --upload-file /root/rpmbuild/RPMS/noarch/python38-dotenv-0.20.0-1.el8.noarch.rpm \
        http://nexus.home.nodeto.com:8081/repository/yum/
