%define version 0.20.0
%define src_name python-dotenv

Name:           python38-dotenv
Version:        %{version}
Release:        1%{?dist}
Summary:        Python dotenv package
License:        GPL
BuildArch:      noarch
Source:         http://nexus.home.nodeto.com:8081/repository/generic/python/%{src_name}-%{version}.tar.gz

Requires:       python38

BuildRequires:  python38
BuildRequires:  python38-pip

%description
The dotenv python library

%prep
%setup -q -n %{src_name}-%{version}

%build
true

%install
/usr/bin/python3.8 -m pip install --target "%{buildroot}%{_libdir}/python3.8/site-packages" .

%files
%{_libdir}/python3.8/site-packages/*

%changelog
